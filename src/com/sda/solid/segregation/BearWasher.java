package com.sda.solid.segregation;

public interface BearWasher {
    void washTheBear();
}
