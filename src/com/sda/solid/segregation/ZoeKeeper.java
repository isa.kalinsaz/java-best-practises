package com.sda.solid.segregation;

public class ZoeKeeper implements BearFeeder, BearWasher {

    @Override
    public void feedTheBear() {

    }

    @Override
    public void washTheBear() {

    }

}
