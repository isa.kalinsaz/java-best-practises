package com.sda.solid.segregation;

public interface BearFeeder {
    void feedTheBear();
}
