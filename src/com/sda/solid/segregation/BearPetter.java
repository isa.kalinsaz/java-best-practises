package com.sda.solid.segregation;

public interface BearPetter {
    void petTheBear();
}
