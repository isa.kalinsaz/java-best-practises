package com.sda.solid.liskov;

public class LiskovExample {


    public static void main(String[] args) {

        Car electricCar = new ElectricCar();
        electricCar.accelerate();

        MotorCarInterface motorCar = new MotorCar();
        motorCar.turnOnEngine();
        motorCar.accelerate();

    }

}
