package com.sda.solid.liskov;

public interface MotorCarInterface extends Car {
    void turnOnEngine();
}
