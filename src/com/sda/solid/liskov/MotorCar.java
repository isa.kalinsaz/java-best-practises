package com.sda.solid.liskov;

public class MotorCar implements MotorCarInterface {

    @Override
    public void turnOnEngine() {
        System.out.println(" Motor engine ON");
    }

    @Override
    public void accelerate() {
        System.out.println("Motor Car Accelerated");
    }

}
