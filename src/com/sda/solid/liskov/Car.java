package com.sda.solid.liskov;

public interface Car {

    void accelerate();

}
