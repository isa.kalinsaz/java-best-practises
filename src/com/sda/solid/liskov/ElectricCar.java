package com.sda.solid.liskov;

public class ElectricCar implements Car {

    @Override
    public void accelerate() {
        System.out.println("Electric Car accelerated!");
    }

}
