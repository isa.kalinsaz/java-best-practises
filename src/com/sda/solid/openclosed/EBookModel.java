package com.sda.solid.openclosed;

import com.sda.solid.singleresponsibility.BookModel;
import lombok.Data;

@Data
public class EBookModel extends BookModel {

    private String url;

}
