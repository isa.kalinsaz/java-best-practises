package com.sda.solid.singleresponsibility;

import com.sda.solid.singleresponsibility.BookModel;
import lombok.Data;

@Data
public class BookOperation {

    private BookModel bookModel;

    public String replaceWordInText(String word) {
        return bookModel.getText().replaceAll(word, bookModel.getText());
    }

    public boolean isWordInText(String word) {
        return bookModel.getText().contains(word);
    }

}
