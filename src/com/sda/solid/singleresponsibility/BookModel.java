package com.sda.solid.singleresponsibility;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BookModel {

    private String name;
    private String author;
    private String text;
    private LocalDate publishDate;

}
