package com.sda.solid.singleresponsibility;

import com.sda.solid.openclosed.EBookModel;
import com.sda.solid.singleresponsibility.BookModel;
import com.sda.solid.singleresponsibility.BookOperation;

import java.time.LocalDate;

public class SingleResponsibilityExample {

    public static void main(String[] args) {

        BookModel bookModel = new BookModel();
        bookModel.setName("Harry Potter");
        bookModel.setAuthor("Isa Kalinsaz");
        bookModel.setText("bla bla blaaaa");
        bookModel.setPublishDate(LocalDate.now());


        EBookModel eBookModel = new EBookModel();
        bookModel.setName("SOLID Principle");
        bookModel.setAuthor("Isa Kalinsaz");
        bookModel.setPublishDate(LocalDate.now());
        bookModel.setText("The SOLID principles of object-oriented design.");
        eBookModel.setUrl("https://www.baeldung.com/solid-principles");


        BookOperation bookOperation = new BookOperation();
        bookOperation.setBookModel(bookModel);

        System.out.println(bookOperation.isWordInText("bla"));

    }

}
