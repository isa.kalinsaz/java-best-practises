package com.sda.solid.dependencyinversion;

import lombok.Data;

@Data
public class Windows98Machine {

    private Keyboard keyboard;
    private Monitor monitor;

}
