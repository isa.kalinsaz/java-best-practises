package com.sda.solid.dependencyinversion;

public class DependencyInversionExample {

    public static void main(String[] args) {

        Keyboard wirelessKeyboard = new WirelessKeyboard();
        Monitor uhdMonitor = new UHDMonitor();
        Windows98Machine windows98Machine = new Windows98Machine();
        windows98Machine.setKeyboard(wirelessKeyboard);
        windows98Machine.setMonitor(uhdMonitor);

        Keyboard standardKeyboard = new StandardKeyboard();
        Monitor lcdMonitor = new LCDMonitor();
        Windows98Machine newWindows98Machine = new Windows98Machine();
        newWindows98Machine.setMonitor(lcdMonitor);
        newWindows98Machine.setKeyboard(standardKeyboard);

    }

}
