package com.sda.solid.dependencyinversion;

import java.util.List;

public class WirelessKeyboard implements Keyboard {
    @Override
    public void openSound() {

    }

    @Override
    public void turnOn() {

    }

    @Override
    public void shutDown() {

    }

    @Override
    public Integer returnAsciiCode(final Character character) {
        return null;
    }

    @Override
    public List<String> showFunctions() {
        return null;
    }
}
