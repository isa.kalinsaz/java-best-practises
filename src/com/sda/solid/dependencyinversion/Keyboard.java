package com.sda.solid.dependencyinversion;

import java.util.List;

public interface Keyboard {

    void openSound();

    void turnOn();

    void shutDown();

    Integer returnAsciiCode(Character character);

    List<String> showFunctions();

}
