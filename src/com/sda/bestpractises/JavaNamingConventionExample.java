package com.sda.bestpractises;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class JavaNamingConventionExample {

    public final static double PI = 3.14;
    public final static int AGE_LIMIT = 10;

    private String name;
    private LocalDate birthDate;
    private LocalDateTime expirationDate;
    private int counter;
    private String carColor = "RED";
    private Color vehicleColor = Color.RED;

    private String newName;

    private static int calculateAge(final LocalDate birthDate) {
        return LocalDate.now().getYear() - birthDate.getYear();
    }

}
