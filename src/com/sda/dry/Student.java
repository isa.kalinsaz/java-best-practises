package com.sda.dry;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Student extends Person {

    private Integer studentId;
    private String className;

    public Student(Integer studentId, String className, String name, String surname, Integer age, String schoolName) {
        super(name, surname, age, schoolName);
        this.studentId = studentId;
        this.className = className;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("studentId=").append(studentId);
        sb.append(", className='").append(className).append('\'');
        sb.append(", Person='").append(super.toString()).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
