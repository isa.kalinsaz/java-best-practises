package com.sda.dry;

import lombok.Data;

@Data
public class Teacher extends Person {

    private String branch;

    public Teacher(String branch, String name, String surname, Integer age, String schoolName) {
        super(name, surname, age, schoolName);
        this.branch = branch;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Teacher{");
        sb.append("Branch='").append(branch).append('\'');
        sb.append(", Person='").append(super.toString()).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
