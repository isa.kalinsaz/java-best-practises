package com.sda.dry;

public class DryExample {

    public static void main(String[] args) {

        Student studentWithConstructor = new Student(123, "Java EE 8", "Aare", "Kulama", 36, "SDA Academy");
        Student studentWithConstructor2 = new Student(123, "Java EE 8", "Aare", "Kulama", 36, "SDA Academy");

        System.out.println(studentWithConstructor);

        Teacher teacherWithConstructor = new Teacher("Computer Science/ Java Architect", "Isa", "Kalinsaz", 32, "SDA Academy");
        System.out.println(teacherWithConstructor);

    }

}
