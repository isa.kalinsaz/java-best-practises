package com.sda.designpatterns.structral.adapter;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoundHole {

    private int radius;

    public boolean fits(RoundPeg peg) {
        return (this.getRadius() >= peg.getRadius());
    }

}
