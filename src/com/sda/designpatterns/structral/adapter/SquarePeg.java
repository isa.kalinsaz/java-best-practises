package com.sda.designpatterns.structral.adapter;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class SquarePeg {
    private double width;

    public double getArea() {
        return Math.pow(this.width, 2);
    }
}
