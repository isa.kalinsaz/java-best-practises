package com.sda.designpatterns.structral.adapter;

public class AdapterPatternExample {

    public static void main(String[] args) {

        RoundHole roundHole = new RoundHole(5);
        RoundPeg roundPeg = new RoundPeg(6);

        if (roundHole.fits(roundPeg)) {
            System.out.println("Round peg fits round hole.");
        } else {
            System.out.println("Round peg does not fit round hole.");
        }

        SquarePeg squarePeg = new SquarePeg(12);
        System.out.println(squarePeg.getWidth());
        System.out.println("Square Peg area is :" + squarePeg.getArea());

        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(squarePeg);

        if (roundHole.fits(squarePegAdapter)) {
            System.out.println("Square peg fits round hole.");
        } else {
            System.out.println("Square peg does not fit round hole.");
        }

    }

}
