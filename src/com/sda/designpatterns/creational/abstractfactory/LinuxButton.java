package com.sda.designpatterns.creational.abstractfactory;

public class LinuxButton implements Button {
    @Override
    public void paint() {
        System.out.println("you have created LinuxButton");
    }
}
