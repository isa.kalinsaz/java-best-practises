package com.sda.designpatterns.creational.abstractfactory;

public interface GuiFactory {

    Button createButton();

    Checkbox createCheckbox();
    
}
