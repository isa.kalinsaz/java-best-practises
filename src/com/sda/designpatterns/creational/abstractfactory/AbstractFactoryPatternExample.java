package com.sda.designpatterns.creational.abstractfactory;

public class AbstractFactoryPatternExample {

    public static void main(String[] args) {

        GuiFactory windowsGuiFactory = new WindowsFactory();
        Application windowsApplication = new Application(windowsGuiFactory);
        windowsApplication.print();

        GuiFactory macOsGuiFactory = new MacOsFactory();
        Application macOsApplication = new Application(macOsGuiFactory);
        macOsApplication.print();

        GuiFactory linuxGuiFactory = new LinuxFactory();
        Application linuxApplication = new Application(linuxGuiFactory);
        linuxApplication.print();
    }
}
