package com.sda.designpatterns.creational.abstractfactory;

public class Application {

    private Button button;
    private Checkbox checkbox;

    public Application(GuiFactory guiFactory) {
        button = guiFactory.createButton();
        checkbox = guiFactory.createCheckbox();
    }

    public void print() {
        button.paint();
        checkbox.print();
    }


}
