package com.sda.designpatterns.creational.abstractfactory;

public class LinuxCheckbox implements Checkbox {
    @Override
    public void print() {
        System.out.println("You have created LinuxCheckbox.");
    }
}
