package com.sda.designpatterns.creational.abstractfactory;

public class MacOsCheckbox implements Checkbox {

    @Override
    public void print() {
        System.out.println("You have created MacOsCheckbox.");
    }

}
