package com.sda.designpatterns.creational.singleton;

public enum Singleton {
    INSTANCE
}
