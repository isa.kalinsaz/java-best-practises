package com.sda.designpatterns.creational.singleton;

public class SingleObject {

    private final static SingleObject object = new SingleObject();

    private SingleObject() {
        System.out.println("This is private constructor!");
    }

    public static SingleObject getInstance() {
        return object;
    }

}
