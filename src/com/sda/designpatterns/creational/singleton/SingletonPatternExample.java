package com.sda.designpatterns.creational.singleton;

public class SingletonPatternExample {

    public static void main(String[] args) {

        SingleObject singleObject = SingleObject.getInstance();
        SingleObject newSingleObject = SingleObject.getInstance();

        //System.out.println(singleObject);
        //System.out.println(newSingleObject);


        AlternativeSingleton alternativeSingleton = AlternativeSingleton.getInstance();
        AlternativeSingleton alternativeSingleton2 = AlternativeSingleton.getInstance();

        //System.out.println(alternativeSingleton);
        //System.out.println(alternativeSingleton2);

        EarlySingletonInitialization earlySingletonInitialization = EarlySingletonInitialization.getInstance();
        EarlySingletonInitialization earlySingletonInitialization2 = EarlySingletonInitialization.getInstance();

        //System.out.println(earlySingletonInitialization);
        //System.out.println(earlySingletonInitialization2);

        Singleton instance = Singleton.INSTANCE;
        Singleton instance1 = Singleton.INSTANCE;

        System.out.println(instance);
        System.out.println(instance1);

    }

}
