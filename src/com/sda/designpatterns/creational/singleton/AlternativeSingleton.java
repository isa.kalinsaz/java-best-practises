package com.sda.designpatterns.creational.singleton;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AlternativeSingleton {

    private static AlternativeSingleton alternativeSingleton;


    public static AlternativeSingleton getInstance() {

        if (alternativeSingleton == null) {
            synchronized (AlternativeSingleton.class) {
                if (alternativeSingleton == null) {
                    alternativeSingleton = new AlternativeSingleton();
                }
            }
        }

        return alternativeSingleton;
    }

}
