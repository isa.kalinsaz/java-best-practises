package com.sda.designpatterns.creational.singleton;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EarlySingletonInitialization {

    private final static EarlySingletonInitialization INSTANCE = new EarlySingletonInitialization();

    public static EarlySingletonInitialization getInstance() {
        return INSTANCE;
    }

}
