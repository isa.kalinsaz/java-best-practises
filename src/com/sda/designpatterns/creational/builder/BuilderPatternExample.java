package com.sda.designpatterns.creational.builder;

public class BuilderPatternExample {

    public static void main(String[] args) {

        House emptyHouse = new House();

        //House houseWithoutFloor = new House.HouseBuilder().door(1).window(2).build();

        House fullHouse = House.builder().door(4).floor(2).window(1).build();
        House houseWithoutFloor = House.builder().door(4).window(1).build();

        System.out.println(emptyHouse);
        System.out.println(houseWithoutFloor);
        System.out.println(fullHouse);

        Student student = Student.builder().firstName("Isa").lastName("Kalinsaz").build();
        System.out.println(student);
    }
}
