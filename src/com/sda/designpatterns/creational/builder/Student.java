package com.sda.designpatterns.creational.builder;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Student {

    private String firstName;
    private String lastName;
    private int age;
    private String phone;
    private String address;

}
