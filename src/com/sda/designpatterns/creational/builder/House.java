package com.sda.designpatterns.creational.builder;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class House {

    private int window;
    private int door;
    private int room;
    private int floor;

    private boolean hasGarage;
    private boolean hasSwimmingPool;
    private boolean hasStatue;
    private boolean hasGarden;


    public static House.HouseBuilder builder() {
        return new HouseBuilder();
    }

    public static class HouseBuilder {

        private House house;

        private HouseBuilder() {
            house = new House();
        }

        public HouseBuilder door(int door) {
            house.setDoor(door);
            return this;
        }

        public HouseBuilder window(int window) {
            house.setWindow(window);
            return this;
        }

        public HouseBuilder floor(int floor) {
            house.setFloor(floor);
            return this;
        }

        public House build() {

            return house;
        }
    }

}

