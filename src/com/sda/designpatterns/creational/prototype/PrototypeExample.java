package com.sda.designpatterns.creational.prototype;

import com.sda.bestpractises.Color;

public class PrototypeExample {
    public static void main(String[] args) {

        Circle circle = new Circle();
        circle.setRadius(10);
        circle.setColor(Color.RED);

        Circle anotherCircle = (Circle) circle.clone();


        System.out.println(circle.equals(anotherCircle));


        Triangle triangle = new Triangle();
        triangle.setA(10);
        triangle.setB(20);

        Triangle sameTriangle = triangle;
        Triangle clonedTriangle = triangle.clone();

        System.out.println(triangle);
        System.out.println(sameTriangle);
        System.out.println(clonedTriangle);

        System.out.println(triangle.equals(sameTriangle));
        System.out.println(triangle.equals(clonedTriangle));

        triangle.equals(circle);

    }
}
