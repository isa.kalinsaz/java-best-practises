package com.sda.designpatterns.creational.prototype;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class Triangle {

    private int a;
    private int b;

    public Triangle() {

    }

    public Triangle(Triangle target) {
        this.a = target.a;
        this.b = target.b;
    }

    public Triangle clone() {
        return new Triangle(this);
    }

    @Override
    public boolean equals(final Object target) {

        if (this == target)
            return true;

        if (target == null || getClass() != target.getClass())
            return false;

        final Triangle triangle = (Triangle) target;
        return a == triangle.a && b == triangle.b;
    }

}
