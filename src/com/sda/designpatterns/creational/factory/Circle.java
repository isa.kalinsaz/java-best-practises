package com.sda.designpatterns.creational.factory;

public class Circle implements Shape {

    public Circle() {
        System.out.println("Circle shape is constructed!");
    }
    
}
