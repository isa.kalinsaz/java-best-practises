package com.sda.designpatterns.creational.factory;

public class FactoryPatternExample {

    public static void main(String[] args) {

        Shape circle = ShapeFactory.getInstance(ShapeType.CIRCLE);
        Shape square = ShapeFactory.getInstance(ShapeType.SQUARE);
        Shape rectangle = ShapeFactory.getInstance(ShapeType.RECTANGLE);
        //Shape triangle = ShapeFactory.getInstance(ShapeType.TRIANGLE);

        System.out.println(circle);
        System.out.println(square);
        System.out.println(rectangle);

    }

}
