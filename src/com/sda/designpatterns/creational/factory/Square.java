package com.sda.designpatterns.creational.factory;

public class Square implements Shape {

    public Square() {
        System.out.println("Square shape is constructed!");
    }

}
