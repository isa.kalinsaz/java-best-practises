package com.sda.designpatterns.creational.factory;

public enum ShapeType {
    CIRCLE,
    SQUARE,
    RECTANGLE,
    TRIANGLE
}
