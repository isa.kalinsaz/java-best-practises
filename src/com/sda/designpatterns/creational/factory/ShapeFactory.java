package com.sda.designpatterns.creational.factory;

public class ShapeFactory {

    public static Shape getInstance(ShapeType shapeType) {

        if (ShapeType.CIRCLE.equals(shapeType)) {
            return new Circle();
        } else if (ShapeType.RECTANGLE.equals(shapeType)) {
            return new Rectangle();
        } else if (ShapeType.SQUARE.equals(shapeType)) {
            return new Square();
        } else {
            throw new IllegalArgumentException("The given shape type : " + shapeType.name() + " is not supported by this factory!");
        }

    }


}
