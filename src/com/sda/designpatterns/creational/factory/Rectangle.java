package com.sda.designpatterns.creational.factory;

public class Rectangle implements Shape {

    public Rectangle() {
        System.out.println("Rectangle shape is constructed!");
    }

}
